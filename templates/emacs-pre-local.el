(setq-default

 ;; "Select all the layers you want enabled from `clojure' `scala' `python' `haskell' `web' `cpp'"
 seartipy-layers '()

 seartipy-linux-font "Ubuntu Mono 13"
 seartipy-mac-font "Monaco 13"
 seartipy-windows-font "Consolas 13"

 ;; Select one of the themes from
 ;; `material',
 ;; `material-light',
 ;; `spacemacs-dark',
 ;; `spacemacs-light',
 ;; `solarized-dark',
 ;; `solarized-light'
 seartipy-theme 'darkokai

 ;; One of `vim', `emacs' or . Evil is always enabled but if the
 ;; variable is `emacs' then the `evil-emacs-state' is the default state.'
 seartipy-default-editing-style 'emacs

 seartipy-fullscreen-at-startup nil
 seartipy-line-numbers nil

 ;; Choose what to save amongst `desktop' `savehist' `saveplace' `recentf' `workspace'
 seartipy-save-session '(savehist saveplace recentf)

 ;; "If not nil,lispy is enabled"
 seartipy-lispy nil
 )
