# My Personal Emacs config

## Quick Installation

Backup your current `.emacs.d` directory

        mv ~/.emacs.d ~/.emacs.d.backup

Clone this repository as `.emacs.d`

        git clone https://gitlab.com/seartipy/emacsd.git ~/.emacs.d

Just launch emacs and wait for it to install all packages.  That's it!

## Configuration

Enter the follwing command in a terminal

        cp ~/.emacs.d/templates/emacs-pre-local.el ~/.emacs-pre-local.el

Then edit `~/.emacs-pre-local.el` to customize this configuration.

You could also create `~/.emacs-post-local.el` to add your own `elisp` code.
