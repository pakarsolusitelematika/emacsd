(defvar seartipy-layers '()
  "Select the layers you want enabled from `clojure' `scala' `python' `haskell' `web' `cpp'")

(defvar seartipy-theme 'material
  "Select one of the themes from
`material',
`material-light',
`spacemacs-dark',
`spacemacs-light',
`solarized-dark',
`solarized-light'")

(defvar seartipy-leader-key "SPC"
  "The leader key. Use `M-<leader>' in `emacs state' and `insert state'")

(defvar seartipy-emacs-leader-key "M-m"
  "The leader key accessible in `emacs state' and `insert state'")

(defvar seartipy-major-mode-leader-key "RET"
  "Major mode leader key is a shortcut key which is the equivalent of
pressing `<leader> m`. Use `M-<leader>' in `emacs state' and `insert state'")

(defvar seartipy-major-mode-emacs-leader-key "C-M-m"
  "Major mode leader key accessible in `emacs state' and `insert state'")

(defvar  seartipy-default-editing-style 'emacs
  "One of `vim', `emacs' or `hybrid' . Evil is always enabled but if the variable is `emacs' then the `evil-emacs-state' is the default state.'")

(defvar seartipy-linux-font "Ubuntu Mono 13"
  "Default font")

(defvar seartipy-mac-font "Monaco 13"
  "Default font")

(defvar seartipy-windows-font "Consolas 13"
  "Default font")

(defvar seartipy-fullscreen-at-startup nil
  "If non nil the frame is fullscreen when Emacs starts up (Emacs 24.4+ only).")

(defvar seartipy-line-numbers nil
  "Set it to `t' if you want to enable line numbers by default")

(defvar seartipy-highlight-line nil
  "Set it to `t' if you want to highlight curent line by default")

(defvar seartipy-save-session '(savehist saveplace recentf)
  "Choose what to save amongst `desktop' `savehist' `saveplace' `recentf' `workspace'")

(defvar seartipy-lispy nil
  "If not nil,lispy is enabled")

(defvar seartipy-super-key nil
  "If not nil, super keybindings will be enabled")

(provide 'init-config)
