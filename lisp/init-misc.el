(use-package pcre2el
  :defer t

  :init
  (my/declare-prefixes
    "R" "pcre2el"
    "R" "pcre2el")

  (evil-leader/set-key
    "R/"  'rxt-explain
    "Rc"  'rxt-convert-syntax
    "Rx"  'rxt-convert-to-rx
    "R'"  'rxt-convert-to-strings
    "Rpe" 'rxt-pcre-to-elisp
    "R%"  'pcre-query-replace-regexp
    "Rpx" 'rxt-pcre-to-rx
    "Rps" 'rxt-pcre-to-sre
    "Rp'" 'rxt-pcre-to-strings
    "Rp/" 'rxt-explain-pcre
    "Re/" 'rxt-explain-elisp
    "Rep" 'rxt-elisp-to-pcre
    "Rex" 'rxt-elisp-to-rx
    "Res" 'rxt-elisp-to-sre
    "Re'" 'rxt-elisp-to-strings
    "Ret" 'rxt-toggle-elisp-rx
    "Rt"  'rxt-toggle-elisp-rx))

(use-package page-break-lines
  :diminish page-break-lines-mode
  :defer t

  :init
  (add-hook 'emacs-lisp-mode-hook 'page-break-lines-mode)
  (add-hook 'sh-mode-hook 'page-break-lines-mode))

(use-package fullframe :defer t)

(use-package noflet
  :config
  ;; do not ask to kill processes
  (defadvice save-buffers-kill-emacs (around no-query-kill-emacs activate)
    (noflet ((process-list ())) ad-do-it)))

(use-package markdown-mode :mode "\\.\\(md\\|markdown\\)\\'")

(use-package esup :defer t)

(use-package restart-emacs
  :defer t

  :init
  (defun reboot-emacs (&optional args)
    "Restart emacs."
    (interactive)
    (restart-emacs args))

  (defun reboot-emacs-resume-layouts (&optional args)
    "Restart emacs and resume layouts."
    (interactive)
    (restart-emacs (cons "--resume-layouts" args)))

  (defun reboot-emacs-debug-init (&optional args)
    "Restart emacs and enable debug-init."
    (interactive)
    (restart-emacs (cons "--debug-init" args)))

  (evil-leader/set-key
    "qq" 'kill-emacs
    "qd" 'reboot-emacs-debug-init
    "qr" 'reboot-emacs-resume-layouts
    "qR" 'reboot-emacs))

(provide 'init-misc)
