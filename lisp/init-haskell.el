

;;;; haskell

;;TODO: hindent, shm

(use-package haskell-mode
  :defer t
  :init
  (if (fboundp 'electric-indent-local-mode)
      (electric-indent-local-mode -1))
  (add-auto-mode 'haskell-mode "\\.ghci\\'")

  ;; Make compilation-mode understand "at blah.hs:11:34-50" lines output by GHC
  (with-eval-after-load 'compile
    (let ((alias 'ghc-at-regexp))
      (add-to-list
       'compilation-error-regexp-alist-alist
       (list alias " at \\(.*\\.\\(?:l?[gh]hs\\|hi\\)\\):\\([0-9]+\\):\\([0-9]+\\)-[0-9]+$" 1 2 3 0 1))
      (add-to-list
       'compilation-error-regexp-alist alias)))

  ;; (add-hook 'inferior-haskell-mode-hook 'turn-on-ghci-completion)
  ;; (add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
  ;; (setq-default haskell-stylish-on-save t)

  (my/declare-prefixes-for-mode 'haskell-mode
    "g" "haskell/navigation"
    "s" "haskell/repl"
    "c" "haskell/cabal"
    "h" "haskell/documentation"
    "d" "haskell/debug"
    "s" "haskell/repl"
    "s" "haskell/repl"

    "g" "haskell/navigation"
    "s" "haskell/repl"
    "c" "haskell/cabal"
    "h" "haskell/documentation"
    "d" "haskell/debug"
    "s" "haskell/repl")

  ;; Switch back to editor from REPL
  (evil-leader/set-key-for-mode haskell-interactive-mode
    "sS"  'haskell-interactive-switch-back)

  ;; Compile
  (evil-leader/set-key-for-mode haskell-cabal
    "C"  'haskell-compile)

  ;; Cabal-file bindings
  (evil-leader/set-key-for-mode haskell-cabal-mode
    ;; "="   'haskell-cabal-subsection-arrange-lines ;; Does a bad job, 'gg=G' works better
    "d"   'haskell-cabal-add-dependency
    "b"   'haskell-cabal-goto-benchmark-section
    "e"   'haskell-cabal-goto-executable-section
    "t"   'haskell-cabal-goto-test-suite-section
    "m"   'haskell-cabal-goto-exposed-modules
    "l"   'haskell-cabal-goto-library-section
    "n"   'haskell-cabal-next-subsection
    "p"   'haskell-cabal-previous-subsection
    "sc"  'haskell-interactive-mode-clear
    "sS"  'haskell-interactive-switch
    "N"   'haskell-cabal-next-section
    "P"   'haskell-cabal-previous-section
    "f"   'haskell-cabal-find-or-create-source-file)

  ;;GHCi-ng
  (when (executable-find "ghci-ng")
    ;; haskell-process-type is set to auto, so setup ghci-ng for either case
    ;; if haskell-process-type == cabal-repl
    (setq haskell-process-args-cabal-repl '("--ghc-option=-ferror-spans" "--with-ghc=ghci-ng"))
    ;; if haskell-process-type == GHCi
    (setq haskell-process-path-ghci "ghci-ng")
    ;; fixes ghci-ng for stack projects
    (setq haskell-process-wrapper-function
          (lambda (args)
            (append args (list "--with-ghc" "ghci-ng")))))

  (with-eval-after-load 'haskell-cabal-mode-map
    (define-key haskell-cabal-mode-map
      [?\C-c ?\C-z] 'haskell-interactive-switch))

  (dolist (hook '(haskell-mode-hook inferior-haskell-mode-hook haskell-interactive-mode-hook))
    ;; (add-hook hook '-on-haskell-doc-mode)
    (add-hook hook (lambda ()
                     (company-mode)
                     (subword-mode)
                     (eldoc-mode))))

  :config
  ;; configure C-c C-l so it doesn't throw any errors
  (bind-key "C-c C-l" 'haskell-process-load-file haskell-mode-map)

  (evil-leader/set-key-for-mode haskell-mode
    "gg"  'haskell-mode-jump-to-def-or-tag
    "gi"  'haskell-navigate-imports
    "f"   'haskell-mode-stylish-buffer

    "sb"  'haskell-process-load-file
    "sc"  'haskell-interactive-mode-clear
    "sS"  'haskell-interactive-switch

    "ca"  'haskell-process-cabal
    "cb"  'haskell-process-cabal-build
    "cc"  'haskell-compile
    "cv"  'haskell-cabal-visit-file

    "hd"  'inferior-haskell-find-haddock
    "hh"  'hoogle
    "hH"  'haskell-hoogle-lookup-from-local
    "hi"  (lookup-key haskell-mode-map (kbd "C-c TAB"))
    "ht"  (lookup-key haskell-mode-map (kbd "C-c C-t"))
    "hy"  'hayoo

    "dd"  'haskell-debug
    "db"  'haskell-debug/break-on-function
    "dn"  'haskell-debug/next
    "dN"  'haskell-debug/previous
    "dB"  'haskell-debug/delete
    "dc"  'haskell-debug/continue
    "da"  'haskell-debug/abandon
    "dr"  'haskell-debug/refresh))

(use-package company-ghc
  :defer t
  :init
  (with-eval-after-load 'company
    (add-to-list 'company-backends 'company-ghc)))

(use-package ghc
  :defer t
  :init
  (add-hook 'haskell-mode-hook 'ghc-init)

  :config
  (my/declare-prefix-for-mode
    'haskell-mode "m" "haskell/ghc-mod")
  (evil-leader/set-key-for-mode haskell-mode
    "mt" 'ghc-insert-template-or-signature
    "mu" 'ghc-initial-code-from-signature
    "ma" 'ghc-auto
    "mf" 'ghc-refine
    "me" 'ghc-expand-th
    "mn" 'ghc-goto-next-hole
    "mp" 'ghc-goto-prev-hole
    "m>"  'ghc-make-indent-deeper
    "m<"  'ghc-make-indent-shallower)
  ;; remove overlays from ghc-check.el if flycheck is enabled
  (set-face-attribute 'ghc-face-error nil :underline nil)
  (set-face-attribute 'ghc-face-warn nil :underline nil))

(use-package flycheck-haskell
  :defer t
  :init
  (with-eval-after-load 'flycheck
    (add-hook 'haskell-mode-hook #'flycheck-haskell-setup)))

(use-package hayoo :defer t)

(provide 'init-haskell)
