;; Prefer g-prefixed coreutils version of standard utilities when available
(let ((gls (executable-find "gls")))
  (when gls (setq insert-directory-program gls)))

(setq-default grep-highlight-matches t
              grep-scroll-output t)

(when *is-a-mac*
  (setq-default locate-command "mdfind"))

(use-package ag
  :defer t

  :config (setq ag-highlight-search t
                ag-reuse-buffers t))

(use-package wgrep :defer t)

(use-package wgrep-ag
  :defer t

  :init
  (autoload 'wgrep-ag-setup "wgrep-ag"))

(use-package anzu
  :diminish anzu-mode

  :config
  (global-anzu-mode))

(use-package helm-ag
  :defer t
  :init
  (custom-set-variables
   '(helm-ag-use-aginore t)
   '(helm-ag-instert-at-point 'symbol))

  (evil-leader/set-key
    "sa" 'helm-ag
    "sA" 'helm-do-ag

    "sf" 'helm-ag-this-file
    "sF" 'helm-do-ag-this-file

    "sb" 'helm-ag-buffers
    "sB" 'helm-do-ag-buffers

    "sp" 'helm-ag-project-root
    "sP" 'helm-do-ag-project-root
    "s`" 'helm-ag-pop-stack))

(use-package helm-swoop
  :defer t

  :init
  (setq helm-swoop-split-with-multiple-windows t
        helm-swoop-split-direction 'split-window-vertically
        helm-swoop-speed-or-color t
        helm-swoop-split-window-function 'helm-default-display-buffer
        helm-swoop-pre-input-function (lambda () ""))

  (evil-leader/set-key
    "ss"    'helm-swoop
    "sS"    'helm-swoop-all
    "s C-s" 'helm-multi-swoop-all))

(provide 'init-search)
