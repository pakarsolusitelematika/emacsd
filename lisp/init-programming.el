(use-package exec-path-from-shell
  :if (not *is-a-windows*)
  :config
  (dolist (var '("SSH_AUTH_SOCK"
                 "SSH_AGENT_PID"
                 "GPG_AGENT_INFO"
                 "LANG"
                 "LC_CTYPE"
                 "JAVA_HOME"
                 "JDK_HOME"
                 "GOPATH"
                 "NVM_DIR"
                 "PYENV_ROOT"
                 "PYTHONPATH"))
    (add-to-list 'exec-path-from-shell-variables var))
  (setq exec-path-from-shell-check-startup-files nil)
  (setenv "SEARTIPY_EMACS" (emacs-version))
  (exec-path-from-shell-initialize))

(use-package multi-term
  :if (not *is-a-windows*)
  :bind (("<f5>" . multi-term-dedicated-toggle))
  :commands (multi-term)

  :init
  (custom-set-variables '(multi-term-dedicated-select-after-open-p t))
  (setq multi-term-scroll-to-bottom-on-output "others")
  (setq multi-term-scroll-show-maximum-output +1)

  :config
  (add-to-list 'term-bind-key-alist '("M-DEL" . term-send-backward-kill-word))
  (add-to-list 'term-bind-key-alist '("M-d" . term-send-forward-kill-word))

  (defun projectile-multi-term-in-root ()
    "Invoke `multi-term' in the project's root."
    (interactive)
    (projectile-with-default-dir (projectile-project-root) (multi-term)))
  (evil-leader/set-key "p$t" 'projectile-multi-term-in-root))

(use-package eldoc
  :diminish eldoc-mode
  :defer t

  :config
  ;; enable eldoc in `eval-expression'
  (add-hook 'eval-expression-minibuffer-setup-hook #'eldoc-mode))

(when (>= emacs-major-version 25)
  (eval-after-load 'bytecomp
    '(add-to-list 'byte-compile-not-obsolete-funcs
                  'preceding-sexp)))

(use-package eval-sexp-fu)



;;;; Compile

(setq-default compilation-scroll-output t)

(use-package alert
  :commands (alert))

;; Customize `alert-default-style' to get messages after compilation

(defun alert-after-compilation-finish (buf result)
  "Use `alert' to report compilation RESULT if BUF is hidden."
  (unless (catch 'is-visible
            (walk-windows (lambda (w)
                            (when (eq (window-buffer w) buf)
                              (throw 'is-visible t))))
            nil)
    (alert (concat "Compilation " result)
           :buffer buf
           :category 'compilation)))

(with-eval-after-load 'compile
  (add-hook 'compilation-finish-functions
            'alert-after-compilation-finish))

(defvar last-compilation-buffer nil
  "The last buffer in which compilation took place.")

(with-eval-after-load 'compile
  (defadvice compilation-start (after save-compilation-buffer activate)
    "Save the compilation buffer to find it later."
    (setq last-compilation-buffer next-error-last-buffer))

  (defadvice recompile (around find-prev-compilation (&optional edit-command) activate)
    "Find the previous compilation buffer, if present, and recompile there."
    (if (and (null edit-command)
             (not (derived-mode-p 'compilation-mode))
             last-compilation-buffer
             (buffer-live-p (get-buffer last-compilation-buffer)))
        (with-current-buffer last-compilation-buffer
          ad-do-it)
      ad-do-it)))

(bind-key [f6] 'recompile)

(defadvice shell-command-on-region
    (after shell-command-in-view-mode
           (start end command &optional output-buffer replace error-buffer display-error-buffer)
           activate)
  "Put \"*Shell Command Output*\" buffers into view-mode."
  (unless output-buffer
    (with-current-buffer "*Shell Command Output*"
      (view-mode 1))))

(with-eval-after-load 'compile
  (require 'ansi-color)
  (defun colourise-compilation-buffer ()
    (when (eq major-mode 'compilation-mode)
      (ansi-color-apply-on-region compilation-filter-start (point-max))))
  (add-hook 'compilation-filter-hook 'colourise-compilation-buffer))



(use-package flycheck-pos-tip :defer t)

(use-package flycheck
  :diminish flycheck-mode

  :init
  (add-hook 'after-init-hook 'global-flycheck-mode)
  (setq flycheck-check-syntax-automatically '(save new-line mode-enabled)
        flycheck-idle-change-delay 0.8)
  (custom-set-variables
   '(flycheck-display-errors-function #'flycheck-pos-tip-error-messages))
  (flycheck-pos-tip-mode)

  (evil-leader/set-key
    "ec" 'flycheck-clear
    "eh" 'flycheck-describe-checker
    "es" 'flycheck-select-checker
    "eS" 'flycheck-set-checker-executable
    "ev" 'flycheck-verify-setup))



;; Exclude very large buffers from dabbrev
(defun dabbrev-friend-buffer (other-buffer)
  (< (buffer-size other-buffer) (* 1 1024 1024)))
(setq dabbrev-friend-buffer-function 'dabbrev-friend-buffer)



(use-package company
  :diminish company-mode " ⓐ"
  :defer t

  :init
  (setq company-idle-delay 0.1
        company-tooltip-limit 10
        company-minimum-prefix-length 2
        company-tooltip-flip-when-above t)

  :config
  (add-to-list 'completion-styles 'initials t)
  (setq company-minimum-prefix-length 2))

(use-package company-quickhelp
  :defer t

  :init
  (with-eval-after-load 'company
    (company-quickhelp-mode 1)))

(use-package helm-company
  :defer t

  :init
  (eval-after-load 'company
    '(progn
       (define-key company-mode-map (kbd "C-:") 'helm-company)
       (define-key company-active-map (kbd "C-:") 'helm-company))))



(use-package projectile
  :defer t
  :diminish projectile-mode
  :init
  (setq-default projectile-enable-caching t)
  (setq projectile-sort-order 'recentf)
  (setq projectile-cache-file (concat seartipy-cache-directory
                                      "projectile.cache"))
  (setq projectile-known-projects-file (concat seartipy-cache-directory
                                               "projectile-bookmarks.eld"))
  (evil-leader/set-key
    "p!" 'projectile-run-shell-command-in-root
    "p&" 'projectile-run-async-shell-command-in-root
    "pa" 'projectile-toggle-between-implementation-and-test
    "pc" 'projectile-compile-project
    "pD" 'projectile-dired
    "pG" 'projectile-regenerate-tags
    "pI" 'projectile-invalidate-cache
    "pk" 'projectile-kill-buffers
    "po" 'projectile-multi-occur
    "pR" 'projectile-replace
    "pT" 'projectile-find-test-file
    "py" 'projectile-find-tag)
  :config
  (projectile-global-mode))

(use-package helm-projectile
  :commands (helm-projectile-switch-to-buffer
             helm-projectile-find-dir
             helm-projectile-find-file
             helm-projectile
             helm-projectile-switch-project
             helm-projectile-recentf
             projectile-vc helm-projectile-grep)

  :init
  (with-eval-after-load 'projectile (helm-projectile-on))
  (evil-leader/set-key
    "pb"  'helm-projectile-switch-to-buffer
    "pd"  'helm-projectile-find-dir
    "pf"  'helm-projectile-find-file
    "ph"  'helm-projectile
    "pp"  'helm-projectile-switch-project
    "pr"  'helm-projectile-recentf
    "pv"  'projectile-vc
    "sgp" 'helm-projectile-grep))

(use-package neotree
  :bind ("<f8>" . neotree-toggle)

  :init
  (evil-leader/set-key
    "ft" 'neotree-toggle
    "pt" 'neotree-find-project-root))

(use-package yasnippet
  :diminish yas-minor-mode " ⓨ"
  :commands (yas-reload-all yas-minor-mode yas-global-mode)

  :init
  (setq yas-installed-snippets-dir (concat user-emacs-directory "snippets"))
  (add-hook 'prog-mode-hook
            '(lambda ()
               (yas-minor-mode))))

(when *is-a-linux*
  (use-package zeal-at-point
    :bind ("\C-cd" . zeal-at-point)))

(use-package dash-at-point
  :defer t

  :init
  (my/declare-prefix "d" "dash")
  (evil-leader/set-key
    "dd" 'dash-at-point
    "dD" 'dash-at-point-with-docset))

(use-package helm-dash
  :defer t

  :init
  (evil-leader/set-key
    "dh" 'helm-dash-at-point
    "dH" 'helm-dash)

  :config
  (defun dash//activate-package-docsets (path)
    "Add dash docsets from specified PATH."
    (setq helm-dash-docsets-path path
          helm-dash-common-docsets (helm-dash-installed-docsets))
    (message (format "activated %d docsets from: %s"
                     (length helm-dash-common-docsets) path)))
  (dash//activate-point-package-docsets dash-helm-dash-docset-path))

(provide 'init-programming)
