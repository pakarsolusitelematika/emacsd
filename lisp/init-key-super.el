(use-package seartipy-super-keybindings
  :ensure nil
  :if seartipy-super-key
  :bind (
         ( "s-'" . evil-avy-goto-char-in-line)
         ( "s-q" . save-buffers-kill-terminal)
         ( "s-v" . yank)
         ( "s-c" . evil-yank)
         ( "s-a" . mark-whole-buffer)
         ( "s-x" . kill-region)
         ( "s-w" . delete-window)
         ( "s-W" . delete-frame)
         ( "s-n" . make-frame)
         ( "s-z" . undo-tree-undo)
         ("s-Z"  . undo-tree-redo)
         ("C-s-f" . toggle-frame-fullscreen))
  :init
  (bind-key "s-=" (lambda () (interactive) (text-scale-increase 1)))
  (bind-key "s--" (lambda () (interactive) (text-scale-decrease 1)))
  (bind-key "s-0" (lambda () (interactive) (text-scale-increase 0)))
  (bind-key "s-s" (lambda ()
                    (interactive)
                    (call-interactively (key-binding "\C-x\C-s"))))

  (provide 'seartipy-super-keybindings))

(provide 'init-key-super)
