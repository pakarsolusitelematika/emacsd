(use-package smartparens
  :diminish smartparens-mode " ⓟ"
  :commands (smartparens-mode smartparens-strict-mode)

  :init
  (setq sp-show-pair-delay 0
        sp-show-pair-from-inside nil
        sp-highlight-pair-overlay nil
        sp-base-key-bindings 'paredit
        sp-autoskip-closing-pair 'always
        sp-hybrid-kill-entire-symbol nil
        sp-cancel-autoskip-on-backward-movement nil)

  :config
  (require 'smartparens-config)
  (sp-use-paredit-bindings)

  (sp-pair "(" ")" :wrap "M-(")
  (sp-pair "\"" "\"" :wrap "M-\"")
  (sp-pair "[" "]" :wrap "M-[")

  ;; (bind-key "s-<delete>" 'sp-kill-sexp smartparens-mode-map)
  ;; (bind-key "s-<backspace>" 'sp-backward-kill-sexp smartparens-mode-map)
  (smartparens-global-mode))

(use-package paxedit
  :defer t
  :diminish paxedit-mode

  :config
  (define-key paxedit-mode-map (kbd "M-<right>") 'paxedit-transpose-forward)
  (define-key paxedit-mode-map (kbd "M-<left>") 'paxedit-transpose-backward)
  (define-key paxedit-mode-map (kbd "M-<up>") 'paxedit-backward-up)
  (define-key paxedit-mode-map (kbd "M-<down>") 'paxedit-backward-end)
  (define-key paxedit-mode-map (kbd "M-b") 'paxedit-previous-symbol)
  (define-key paxedit-mode-map (kbd "M-f") 'paxedit-next-symbol)
  (define-key paxedit-mode-map (kbd "C-%") 'paxedit-copy)
  (define-key paxedit-mode-map (kbd "C-&") 'paxedit-kill)
  (define-key paxedit-mode-map (kbd "C-*") 'paxedit-delete)
  (define-key paxedit-mode-map (kbd "C-^") 'paxedit-sexp-raise)
  ;; ;; Symbol backward/forward kill
  ;; (define-key paxedit-mode-map (kbd "C-w") 'paxedit-backward-kill)
  ;; (define-key paxedit-mode-map (kbd "M-w") 'paxedit-forward-kill)
  ;; ;; Symbol manipulation
  (define-key paxedit-mode-map (kbd "M-u") 'paxedit-symbol-change-case)
  (define-key paxedit-mode-map (kbd "C-@") 'paxedit-symbol-copy)
  (define-key paxedit-mode-map (kbd "C-#") 'paxedit-symbol-kill))

(use-package rainbow-delimiters :defer t)

(use-package lispy
  :defer t
  :diminish lispy-mode

  :init
  (add-hook 'emacs-lisp-mode-hook 'lispy-mode)

  :config
  (if seartipy-lispy
      (lispy-set-key-theme '(special c-digits parinfer))
    (lispy-set-key-theme '(parinfer)))
  (define-key lispy-mode-map (kbd "M-]") 'lispy-forward)
  (define-key lispy-mode-map (kbd "M-}") 'lispy-backward)
  (key-chord-define lispy-mode-map  "]]" (kbd "M-]"))
  (key-chord-define lispy-mode-map "[[" (kbd "M-}")))

(use-package aggressive-indent :defer t)

(add-hook 'emacs-lisp-mode-hook
          '(lambda ()
             (aggressive-indent-mode)
             (company-mode)
             (rainbow-delimiters-mode)
             (lispy-mode)
             (paxedit-mode)
             (smartparens-strict-mode)))

(provide 'init-lispish)
