(use-package undo-tree
  :defer t
  :diminish undo-tree-mode

  :config
  (global-undo-tree-mode))

(use-package highlight-escape-sequences
  :config
  (hes-mode))

(use-package expand-region
  :bind ("C-=" . er/expand-region)

  :init
  (evil-leader/set-key "v" 'er/expand-region)
  (setq expand-region-contract-fast-key "V"
        expand-region-reset-fast-key "r"))

(use-package avy
  :bind (("C-'" . avy-goto-char-2)
         ("M-'" . avy-goto-word-or-subword-1))

  :init
  (evil-leader/set-key
    "SPC" 'avy-goto-word-or-subword-1
    "y" 'avy-goto-line)

  (evil-leader/set-key
    "jb" 'avy-pop-mark
    "jj" 'evil-avy-goto-char
    "jJ" 'evil-avy-goto-char-2
    "jl" 'evil-avy-goto-line
    "jL" 'evil-avy-goto-char-in-line
    "jw" 'evil-avy-goto-word-or-subword-1)

  (setq avy-keys (number-sequence ?a ?z)
        avy-style 'pre
        avy-all-windows nil
        avy-background t)

  :config
  (avy-setup-default)
  (evil-leader/set-key "`" 'avy-pop-mark))

(use-package multiple-cursors
  :bind (
         ("C-c C-c" . mc/edit-lines)
         ("C-c C-e" . mc/edit-ends-of-lines)
         ("C-c C-a" . mc/edit-beginnings-of-lines)
         ("C-<" . mc/mark-previous-like-this)
         ("C->" . mc/mark-next-like-this)

         ("C-c m p" . mc/mark-previous-like-this)
         ("C-c m n" . mc/mark-next-like-this)
         ("C-c m a" . mc/mark-all-like-this)
         ("C-c m e" . mc/edit-ends-of-lines)
         ("C-c m b" . mc/edit-beginnings-of-lines)
         ("C-c m d" . mc/mark-all-dwim)
         ("C-S-<mouse-1>" . mc/add-cursor-on-click))

  :init
  ;; ;; Mark additional regions matching current region
  ;; (bind-key (kbd "M-æ") 'mc/mark-all-dwim)
  ;; (bind-key (kbd "M-å") 'mc/mark-all-in-region)

  ;; ;; Symbol and word specific mark-more
  ;; (bind-key (kbd "s-æ") 'mc/mark-next-word-like-this)
  ;; (bind-key (kbd "s-å") 'mc/mark-previous-word-like-this)
  ;; (bind-key (kbd "M-s-æ") 'mc/mark-all-words-like-this)
  ;; (bind-key (kbd "") 'mc/mark-all-symbols-like-this)

  (custom-set-variables '(mc/always-run-for-all t)))

(use-package move-dup
  :bind (("M-S-<up>" . md/move-lines-up)
         ("M-S-<down>" . md/move-lines-down)
         ("C-c C-p" . md/duplicate-down)
         ("C-c C-P" . md/duplicate-up))

  :init
  (evil-leader/set-key
    "xJ" 'md/move-lines-down
    "xK" 'md/move-lines-up))

(use-package easy-kill
  :defer t

  :init
  (bind-key [remap kill-ring-save] 'easy-kill))

;;repace newlines with spaces
(use-package unfill :defer t)

(use-package editorconfig
  :diminish editorconfig-mode
  :defer t

  :init
  (add-hook 'prog-mode-hook #'editorconfig-mode))

(use-package lorem-ipsum
  :commands (lorem-ipsum-insert-list
             lorem-ipsum-insert-paragraphs
             lorem-ipsum-insert-sentences)
  :init
  (evil-leader/set-key
    "ill" 'lorem-ipsum-insert-list
    "ilp" 'lorem-ipsum-insert-paragraphs
    "ils" 'lorem-ipsum-insert-sentences))

(use-package whitespace-cleanup-mode
  :diminish whitespace-cleanup-mode
  :defer t
  :init
  (add-hook 'after-init-hook 'global-whitespace-cleanup-mode)
  (dolist (hook '(special-mode-hook
                  Info-mode-hook
                  eww-mode-hook
                  term-mode-hook
                  comint-mode-hook
                  compilation-mode-hook
                  magit-popup-mode-hook
                  minibuffer-setup-hook))
    (add-hook hook (lambda ()
                     (setq show-trailing-whitespace nil))))
  (bind-key [remap just-one-space] 'cycle-spacing))

(use-package smooth-scrolling
  :config
  (smooth-scrolling-mode))

(use-package back-button
  :diminish back-button-mode

  :config
  (back-button-mode))

(use-package origami
  :config
  (global-origami-mode)
  (evil-leader/set-key
    "za" 'origami-forward-toggle-node
    "zc" 'origami-close-node
    "zC" 'origami-close-node-recursively
    "zO" 'origami-open-node-recursively
    "zo" 'origami-open-node
    "zr" 'origami-open-all-nodes
    "zm" 'origami-close-all-nodes
    "zs" 'origami-show-only-node
    "zn" 'origami-next-fold
    "zp" 'origami-previous-fold
    "zR" 'origami-reset
    "z TAB" 'origami-recursively-toggle-node))

(use-package ace-jump-mode
  :defer t)

;; similar to vim's f command
(use-package jump-char
  :bind (("M-=" . jump-char-forward)
         ("M-+" . jump-char-backward)))

;; vim's ci and co commands
(use-package change-inner
  :bind (("M-I" . change-inner)
         ("M-O" . change-outer)))

(use-package multifiles
  :bind ("C-!" . mf/mirror-region-in-multifile))

(provide 'init-editing)
