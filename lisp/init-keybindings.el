;; Make <escape> quit as much as possible
(define-key minibuffer-local-map (kbd "<escape>") 'keyboard-escape-quit)
(define-key evil-visual-state-map (kbd "<escape>") 'keyboard-quit)
(define-key minibuffer-local-ns-map (kbd "<escape>") 'keyboard-escape-quit)
(define-key minibuffer-local-completion-map (kbd "<escape>") 'keyboard-escape-quit)
(define-key minibuffer-local-must-match-map (kbd "<escape>") 'keyboard-escape-quit)
(define-key minibuffer-local-isearch-map (kbd "<escape>") 'keyboard-escape-quit)

;; Universal argument ---------------------------------------------------------
(evil-leader/set-key "u" 'universal-argument)
;; shell command  -------------------------------------------------------------
(evil-leader/set-key "!" 'shell-command)



;;;; file

(defun open-in-external-app ()
  "Open current file in external application."
  (interactive)
  (let ((file-path (if (eq major-mode 'dired-mode)
                       (dired-get-file-for-visit)
                     (buffer-file-name))))
    (if file-path
        (cond
         (*is-a-mac* (shell-command (format "open \"%s\"" file-path)))
         (*is-a-linux* (let ((process-connection-type nil))
                         (start-process "" nil "xdg-open" file-path))))
      (message "No file associated to this buffer."))))

(defun show-and-copy-buffer-filename ()
  "Show the full path to the current file in the minibuffer."
  (interactive)
  (let ((file-name (buffer-file-name)))
    (if file-name
        (progn
          (message file-name)
          (kill-new file-name))
      (error "Buffer not visiting a file"))))

(evil-leader/set-key
  "fg" 'rgrep
  "fj" 'dired-jump
  "fo" 'open-in-external-app
  "fR"  'rename-current-buffer-file
  "fS" 'evil-write-all
  "fs" 'evil-write
  "fy" 'show-and-copy-buffer-filename)



;;;; buffer

(use-package buffer-move
  :bind (("<C-S-up>" . buf-move-up)
         ("<C-S-down>" . buf-move-down)
         ("<C-S-left>" . buf-move-left)
         ("<C-S-right>" . buf-move-right))

  :init
  (evil-leader/set-key
    "bmh" 'buf-move-left
    "bmj" 'buf-move-down
    "bmk" 'buf-move-up
    "bml" 'buf-move-right))

(defun switch-to-previous-buffer ()
  "Switch to previously open buffer.
Repeated invocations toggle between the two most recently open buffers."
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))

(defun kill-other-buffers ()
  "Kill all other buffers."
  (interactive)
  (let (name (buffer-name))
    (when (yes-or-no-p (format "Killing all buffers except \"%s\" ? " buffer-file-name))
      (mapc 'kill-buffer (delq (current-buffer) (buffer-list)))
      (message "Buffers deleted!"))))

(defun safe-revert-buffer ()
  "Prompt before reverting the file."
  (interactive)
  (revert-buffer nil nil))

(evil-leader/set-key
  "bd"  'kill-this-buffer
  "TAB" 'switch-to-previous-buffer
  "bK"  'kill-other-buffers
  "bR"  'safe-revert-buffer
  "bw"  'read-only-mode)

(bind-key "C-c b" 'switch-to-previous-buffer)
(bind-key "C-x C-b" 'ibuffer)



(evil-leader/set-key
  "hdb" 'describe-bindings
  "hdc" 'describe-char
  "hdf" 'describe-function
  "hdk" 'describe-key
  "hdp" 'describe-package
  "hdt" 'describe-theme
  "hdv" 'describe-variable
  "hn"  'view-emacs-news)

(evil-leader/set-key
  "xaa" 'align
  "xc"  'count-region
  "xdw" 'delete-trailing-whitespace
  "xtc" 'transpose-chars
  "xtl" 'transpose-lines
  "xtw" 'transpose-words
  "xU"  'upcase-region
  "xu"  'downcase-region)

;; applications
(evil-leader/set-key
  "ac"  'calc-dispatch
  "ad"  'dired
  "ap"  'proced
  "au"  'undo-tree-visualize)

;; errors
(evil-leader/set-key
  "en" 'next-error
  "ep" 'previous-error
  "eN" 'previous-error)

;; Compilation ----------------------------------------------------------------
;;(evil-leader/set-key "cc" 'helm-make-projectile)
(evil-leader/set-key "cC" 'compile)
(evil-leader/set-key "cr" 'recompile)

;; narrow & widen -------------------------------------------------------------
(evil-leader/set-key
  "nr" 'narrow-to-region
  "np" 'narrow-to-page
  "nf" 'narrow-to-defun
  "nw" 'widen)

;; spell check  ---------------------------------------------------------------
(evil-leader/set-key
  "Sd" 'ispell-change-dictionary
  "Sn" 'flyspell-goto-next-error)



;;;; windows

(use-package ace-window
  :defer t

  :init
  (evil-leader/set-key
    "bM"  'ace-swap-window
    "wC"  'ace-delete-window
    "w <SPC>"  'ace-window)

  :config
  (setq aw-dispatch-always t))

;; When splitting window, show (other-buffer) in the new window
(defun split-window-func-with-other-buffer (split-function)
  (lexical-let ((s-f split-function))
    (lambda ()
      (interactive)
      (funcall s-f)
      (set-window-buffer (next-window) (other-buffer)))))

;; rearrange split windows
(defun split-window-horizontally-instead ()
  (interactive)
  (save-excursion
    (delete-other-windows)
    (funcall (split-window-func-with-other-buffer 'split-window-horizontally))))

(defun split-window-vertically-instead ()
  (interactive)
  (save-excursion
    (delete-other-windows)
    (funcall (split-window-func-with-other-buffer 'split-window-vertically))))


;; window functions

(defun switch-to-minibuffer-window ()
  "switch to minibuffer window (if active)"
  (interactive)
  (when (active-minibuffer-window)
    (select-window (active-minibuffer-window))))

(defun split-window-below-and-focus ()
  "Split the window vertically and focus the new window."
  (interactive)
  (split-window-below)
  (windmove-down))

(defun split-window-right-and-focus ()
  "Split the window horizontally and focus the new window."
  (interactive)
  (split-window-right)
  (windmove-right))

(defun layout-triple-columns ()
  " Set the layout to triple columns. "
  (interactive)
  (delete-other-windows)
  (split-window-right)
  (split-window-right)
  (balance-windows))

(defun layout-double-columns ()
  " Set the layout to double columns. "
  (interactive)
  (delete-other-windows)
  (split-window-right))

(defun layout-triple-rows ()
  " Set the layout to triple columns. "
  (interactive)
  (delete-other-windows)
  (split-window-below)
  (split-window-below)
  (balance-windows))

(defun layout-double-rows ()
  " Set the layout to double columns. "
  (interactive)
  (delete-other-windows)
  (split-window-below))

(use-package golden-ratio
  :diminish " ⓖ"
  :defer t)

(evil-leader/set-key
  "w@"  'layout-double-rows
  "w#"  'layout-triple-rows
  "w2"  'layout-double-columns
  "w3"  'layout-triple-columns

  "wb"  'switch-to-minibuffer-window
  "wc"  'delete-window

  "wH"  'evil-window-move-far-left
  "wh"  'evil-window-left
  "wJ"  'evil-window-move-very-bottom
  "wj"  'evil-window-down
  "wK"  'evil-window-move-very-top
  "wk"  'evil-window-up
  "wL"  'evil-window-move-far-right
  "wl"  'evil-window-right

  "wo"  'other-frame
  "wu"  'winner-undo
  "wv"  'split-window-right

  "ws"  'split-window-below
  "wS"  'split-window-below-and-focus
  "w-"  'split-window-below
  "wU"  'winner-redo
  "wV"  'split-window-right-and-focus
  "w/"  'split-window-right

  "ww"  'other-window
  "w="  'balance-windows

  "w_" 'split-window-horizontally-instead
  "w|" 'split-window-vertically-instead)

(use-package winum
  :init
  (evil-leader/set-key
    "`" 'winum-select-window-by-number
    "0" 'winum-select-window-0-or-10
    "1" 'winum-select-window-1
    "2" 'winum-select-window-2
    "3" 'winum-select-window-3
    "4" 'winum-select-window-4
    "5" 'winum-select-window-5
    "6" 'winum-select-window-6
    "7" 'winum-select-window-7
    "8" 'winum-select-window-8
    "9" 'winum-select-window-9)
  :config
  (setq winum-auto-setup-mode-line nil)
  (winum-mode))



;;;; windows

(bind-key "C-x C-2" (split-window-func-with-other-buffer 'split-window-vertically))
(bind-key "C-x C-3" (split-window-func-with-other-buffer 'split-window-horizontally))
(bind-key "\C-x|" 'split-window-horizontally-instead)
(bind-key "\C-x_" 'split-window-vertically-instead)
(bind-key (kbd "C-x o") 'ace-window)



;;;; hydra bindings

(defhydra hydra-move-dup ()
  "move-dup"
  ("k" md/move-lines-up)
  ("j" md/move-lines-down)
  ("K" md/duplicate-up)
  ("J" md/duplicate-down)
  ("<ESC>" nil)
  ("q" nil))

(defhydra hydra-font-zoom ()
  "font-zoom"
  ("+" text-scale-increase "in")
  ("-" text-scale-decrease "out")
  ("=" (text-scale-set 0))
  ("<ESC>" nil "quit")
  ("q" nil "quit"))

(defhydra hydra-splitter ()
  "splitter"
  ("h" hydra-move-splitter-left)
  ("j" hydra-move-splitter-down)
  ("k" hydra-move-splitter-up)
  ("l" hydra-move-splitter-right)
  ("b" balance-windows)
  ("<ESC>" nil "quit")
  ("q" nil "quit"))

(defhydra hydra-movement ()
  "movement"
  ("n" next-line "next")
  ("p" previous-line "prev")
  ("u" scroll-up "up")
  ("d" scroll-down "down")
  ("r" scroll-right "right")
  ("l" scroll-left "left")
  ("<ESC>" nil "quit")
  ("q" nil "quit"))

(defhydra hydra-other-window ()
  "other-window"
  ("n" (scroll-other-window 1) "next line")
  ("p" (scroll-other-window -1) "prev line")
  ("N" scroll-other-window "next page")
  ("P" (scroll-other-window '-) "prev page")
  ("a" (beginning-of-buffer-other-window 0) "begin")
  ("e" (end-of-buffer-other-window 0) "end")
  ("<ESC>" nil "quit")
  ("q" nil "quit"))

;; https://github.com/abo-abo/hydra/wiki/multiple-cursors
(defhydra hydra-multiple-cursors (:hint nil)
  "
     ^Up^            ^Down^        ^Other^
----------------------------------------------
[_p_]   Next    [_n_]   Next    [_l_] Edit lines
[_P_]   Skip    [_N_]   Skip    [_a_] Mark all
[_M-p_] Unmark  [_M-n_] Unmark  [_r_] Mark by regexp
^ ^             ^ ^             [_q_] Quit
"
  ("l" mc/edit-lines :exit t)
  ("a" mc/mark-all-like-this :exit t)
  ("n" mc/mark-next-like-this)
  ("N" mc/skip-to-next-like-this)
  ("M-n" mc/unmark-next-like-this)
  ("p" mc/mark-previous-like-this)
  ("P" mc/skip-to-previous-like-this)
  ("M-p" mc/unmark-previous-like-this)
  ("r" mc/mark-all-in-region-regexp :exit t)
  ("<mouse-1>" mc/add-cursor-on-click)
  ("<down-mouse-1>" ignore)
  ("<drag-mouse-1>" ignore)
  ("q" nil)
  ("<ESC>" nil "quit"))

;; https://github.com/abo-abo/hydra/wiki/Helm-2
(defhydra hydra-helm (:hint nil :color pink)
  "
                                                                          ╭──────┐
   Navigation   Other  Sources     Mark             Do             Help   │ Helm │
  ╭───────────────────────────────────────────────────────────────────────┴──────╯
        ^_k_^         _K_       _p_   [_m_] mark         [_v_] view         [_H_] helm help
        ^^↑^^         ^↑^       ^↑^   [_t_] toggle all   [_d_] delete       [_s_] source help
    _h_ ←   → _l_     _c_       ^ ^   [_u_] unmark all   [_f_] follow: %(helm-attr 'follow)
        ^^↓^^         ^↓^       ^↓^    ^ ^               [_y_] yank selection
        ^_j_^         _J_       _n_    ^ ^               [_w_] toggle windows
  --------------------------------------------------------------------------------
        "
  ("<tab>" helm-keyboard-quit "back" :exit t)
  ("<escape>" nil "quit")
  ("\\" (insert "\\") "\\" :color blue)
  ("h" helm-beginning-of-buffer)
  ("j" helm-next-line)
  ("k" helm-previous-line)
  ("l" helm-end-of-buffer)
  ("g" helm-beginning-of-buffer)
  ("G" helm-end-of-buffer)
  ("n" helm-next-source)
  ("p" helm-previous-source)
  ("K" helm-scroll-other-window-down)
  ("J" helm-scroll-other-window)
  ("c" helm-recenter-top-bottom-other-window)
  ("m" helm-toggle-visible-mark)
  ("t" helm-toggle-all-marks)
  ("u" helm-unmark-all)
  ("H" helm-help)
  ("s" helm-buffer-help)
  ("v" helm-execute-persistent-action)
  ("d" helm-persistent-delete-marked)
  ("y" helm-yank-selection)
  ("w" helm-toggle-resplit-and-swap-windows)
  ("f" helm-follow-mode))

(defhydra hydra-flycheck
  (:pre (progn (setq hydra-lv t) (flycheck-list-errors))
        :post (progn (setq hydra-lv nil) (quit-windows-on "*Flycheck errors*"))
        :hint nil)
  "Errors"
  ("f"  flycheck-error-list-set-filter                            "Filter")
  ("j"  flycheck-next-error                                       "Next")
  ("k"  flycheck-previous-error                                   "Previous")
  ("gg" flycheck-first-error                                      "First")
  ("G"  (progn (goto-char (point-max)) (flycheck-previous-error)) "Last")
  ("q"  nil))

(defhydra hydra-goto-line (goto-map ""
                                    :pre (linum-mode 1)
                                    :post (linum-mode -1))
  "goto-line"
  ("g" goto-line "go")
  ("m" set-mark-command "mark" :bind nil)
  ("q" nil "quit"))

(defhydra hydra-page (ctl-x-map "" :pre (widen))
  "page"
  ("]" forward-page "next")
  ("[" backward-page "prev")
  ("n" narrow-to-page "narrow" :bind nil :exit t)
  ("q" narrow-to-page "narrow" :bind nil :exit t)
  ("ESC" narrow-to-page "narrow" :bind nil :exit t))

(bind-key "C-, l" 'hydra-move-dup/body)
(bind-key "C-, m" 'hydra-movement/body)
(bind-key "C-, o" 'hydra-other-window/body)
(bind-key "C-, s" 'hydra-splitter/body)
(bind-key "C-, z" 'hydra-font-zoom/body)
(bind-key "C-, c" 'hydra-multiple-cursors/body)
(bind-key "C-, h" 'hydra-helm/body)
(bind-key "C-, e" 'hydra-flycheck/body)
(bind-key "C-, p" 'hydra-page/body)



;; Navigate window layouts with "C-c <left>" and "C-c <right>"
(winner-mode)
(windmove-default-keybindings)

(key-chord-define-global "\\\\" 'avy-goto-word-or-subword-1)
(key-chord-define-global "qq" 'avy-goto-char-2)
(key-chord-define-global "QQ" 'avy-goto-word-1)
(key-chord-define-global "zz" 'zap-up-to-char)
(key-chord-define-global "qs" 'cycle-spacing)


(key-chord-define-global "jj" 'avy-goto-word-1)
(key-chord-define-global "jl" 'avy-goto-line)
(key-chord-define-global "jk" 'avy-goto-char)

(key-chord-define-global "JJ" 'crux-switch-to-previous-buffer)
(key-chord-define-global "uu" 'undo-tree-visualize)
(key-chord-define-global "xx" 'execute-extended-command)
(key-chord-define-global "yy" 'browse-kill-ring)

(use-package crux
  :bind (("M-o" . crux-smart-open-line)
         ("C-<backspace>" . crux-kill-line-backwards)
         ("C-c I" . crux-find-user-init-file)
         ("C-c f" . crux-recentf-ido-find-file)
         ("C-c k" . crux-kill-other-buffers)
         ("C-c S" . crux-find-shell-init-file)
         ("C-M-<backspace>" . crux-kill-whole-line))
  :commands (crux-move-beginning-of-line)

  :init
  (bind-key [remap move-beginning-of-line] #'crux-move-beginning-of-line)
  (bind-key [(shift return)] #'crux-smart-open-line)
  (bind-key [(control shift return)] #'crux-smart-open-line-above)
  (bind-key [remap kill-whole-line] #'crux-kill-whole-line))

(evil-leader/set-key
  "nr" 'narrow-to-region
  "np" 'narrow-to-page
  "nf" 'narrow-to-defun
  "nw" 'widen)

(evil-leader/set-key
  "ts" 'smartparens-strict-mode
  "tS" 'smartparens-mode
  "tn" 'neotree-toggle
  "tt" 'multi-term-dedicated-toggle
  "tg" 'golden-ratio-mode
  "th" 'global-hl-line-mode
  "tl" 'linum-mode
  "tf" 'toggle-frame-fullscreen
  "tm" 'toggle-frame-maximized
  "td" 'toggle-debug-on-error
  "tv" 'smooth-scrolling-mode
  "tCd" 'rainbow-delimeters-mode
  "tw" 'whitespace-mode
  "tt" 'focus-mode
  "t C-I" 'aggressive-indent-mode)

;; Zap to char
(autoload 'zap-up-to-char "misc" "Kill up to, but not including ARGth occurrence of CHAR.")
(bind-key "M-z" 'zap-up-to-char)
(bind-key "M-Z" (lambda (char) (interactive "cZap to char: ") (zap-to-char 1 char)))

;; Transpose stuff with M-t
(global-unset-key (kbd "M-t")) ;; which used to be transpose-words
(bind-keys
 ("M-t l" . transpose-lines)
 ("M-t w" . transpose-words)
 ("M-t s" . transpose-sexps)
 ("M-t p" . transpose-params))

;; Vimmy alternatives to M-^ and C-u M-^
(bind-key "C-c j" 'join-line)
(bind-key "C-c J" (lambda () (interactive) (join-line 1)))
(bind-key "M-j" (lambda () (interactive) (join-line -1)))

(bind-key "C-." 'set-mark-command)
(bind-key "C-x C-." 'pop-global-mark)

(bind-key "M-/" 'hippie-expand)

;; Convenient binding for vc-git-grep
(bind-key "C-x v f" 'vc-git-grep)

(when seartipy-super-key
  (require 'init-key-super))

;; Create new frame
(bind-key "C-x C-n" 'make-frame-command)

;; Revert without any fuss
(bind-key "M-ESC" (lambda () (interactive) (revert-buffer t t)))

;; ;; Edit file with sudo
;; (bind-key (kbd "M-s e") 'sudo-edit)

;; Help should search more than just commands
(bind-key "<f1> a" 'apropos)

;; Yank selection in isearch
(bind-key "C-o" 'isearch-yank-x-selection isearch-mode-map)

;; Duplicate region
(bind-key "C-c d" 'md/duplicate-down)
(bind-key "C-c D" 'md/duplicate-up)

;; ;; Toggle quotes
;; (bind-key (kbd "C-\"") 'toggle-quotes)

;; Display and edit occurances of regexp in buffer
(bind-key "C-c o" 'occur)
(bind-key "C-c O" 'multi-occur)
(bind-key "C-c C-o" 'multi-occur-in-matching-buffers)

;; View occurrence in occur mode
(bind-key "v" 'occur-mode-display-occurrence occur-mode-map)
(bind-key "n" 'next-line occur-mode-map)
(bind-key "p" 'previous-line occur-mode-map)

;; ;; I don't need to kill emacs that easily
;; the mnemonic is C-x REALLY QUIT
;; (bind-key "C-x r q" 'save-buffers-kill-terminal)
;; (bind-key "C-x C-c" 'delete-frame)

(provide 'init-keybindings)
