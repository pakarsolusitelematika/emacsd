;; git-gutter like package, but works better with linum-mode
(use-package diff-hl
  :defer t

  :init
  (add-hook 'prog-mode-hook 'turn-on-diff-hl-mode)
  (add-hook 'vc-dir-mode-hook 'turn-on-diff-hl-mode)
  (with-eval-after-load 'dired
    (add-hook 'dired-mode-hook 'diff-hl-dired-mode)))

(use-package magit
  :commands (magit-blame-mode
             magit-commit-popup
             magit-diff-popup
             magit-fetch-popup
             magit-log-popup
             magit-pull-popup
             magit-push-popup
             magit-status)
  :bind (("C-x g" . magit-status)
         ("C-x M-g" . magit-dispatch-popup))
  :init
  (setq-default
   magit-process-popup-time 10
   magit-diff-refine-hunk t)
  (with-eval-after-load 'magit
    (define-key magit-status-mode-map (kbd "C-M-<up>") 'magit-section-up))
  (with-eval-after-load 'magit
    (fullframe magit-status magit-mode-quit-window))

  (my/declare-prefixes "gd" "diff")

  (evil-leader/set-key
    "gc" 'magit-commit-popup
    "gC" 'magit-checkout
    "gd" 'magit-diff-popup
    "ge" 'magit-ediff-compare
    "gE" 'magit-ediff-show-working-tree
    "gf" 'magit-fetch-popup
    "gF" 'magit-pull-popup
    "gi" 'magit-init
    "gl" 'magit-log-popup
    "gL" 'magit-log-buffer-file
    "gP" 'magit-push-popup
    "gs" 'magit-status
    "gS" 'magit-stage-file
    "gU" 'magit-unstage-file))

(use-package git-commit
  :defer t

  :init
  (add-hook 'git-commit-mode-hook 'goto-address-mode)
  (when *is-a-mac*
    (with-eval-after-load 'magit
      (add-hook 'magit-mode-hook (lambda () (local-unset-key [(meta h)]))))))

(use-package git-messenger
  :bind ("C-x v p" . git-messenger:popup-message))

(use-package gitignore-mode :defer t)
(use-package gitconfig-mode :defer t)
(use-package gitattributes-mode :defer t)

(use-package git-timemachine :defer t)

(use-package gist :defer t)
(use-package github-browse-file
  :defer t

  :init
  (evil-leader/set-key
    "gho" 'github-browse-file))

(use-package ibuffer-vc
  :init
  (defun ibuffer-set-up-preferred-filters ()
    (ibuffer-vc-set-filter-groups-by-vc-root)
    (unless (eq ibuffer-sorting-mode 'filename/process)
      (ibuffer-do-sort-by-filename/process)))
  (add-hook 'ibuffer-hook 'ibuffer-set-up-preferred-filters))

(use-package helm-gitignore
  :defer t

  :init (evil-leader/set-key "gI" 'helm-gitignore))

(provide 'init-git)
