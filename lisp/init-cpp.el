

;;;; cpp

(use-package irony
  :diminish irony-mode
  :defer t
  :init
  (custom-set-variables '(irony-additional-clang-options '("-std=c++1z")))
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'objc-mode-hook 'irony-mode)

  ;; replace the `completion-at-point' and `complete-symbol' bindings in
  ;; irony-mode's buffers by irony-mode's function
  (defun my/irony-mode-hook ()
    (company-mode)
    (eldoc-mode)
    (define-key irony-mode-map [remap completion-at-point]
      'irony-completion-at-point-async)
    (define-key irony-mode-map [remap complete-symbol]
      'irony-completion-at-point-async))
  (add-hook 'irony-mode-hook 'my/irony-mode-hook)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options))

(use-package flycheck-irony
  :defer t
  :init
  (with-eval-after-load 'flycheck
    (add-hook 'flycheck-mode-hook #'flycheck-irony-setup)))

(use-package company-irony
  :defer t
  :init
  (with-eval-after-load 'company
    (add-to-list 'company-backends 'company-irony)))

(use-package irony-eldoc
  :defer t
  :init
  (add-hook 'irony-mode-hook 'irony-eldoc))

(use-package disaster
  :defer t)

(use-package clang-format
  :defer t)

(provide 'init-cpp)
