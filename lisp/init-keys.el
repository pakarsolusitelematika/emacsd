;;;; evil

(use-package evil
  :defer t
  :init
  (when (eq seartipy-default-editing-style 'emacs)
    (setq evil-default-state 'emacs))

  (setq evil-normal-state-cursor '("DarkGoldenrod2" box))
  (setq evil-insert-state-cursor '("chartreuse3" (bar . 2)))
  (setq evil-hybrid-state-cursor '("SkyBlue2" (bar . 2)))
  (setq evil-emacs-state-cursor '("SkyBlue2" box))
  (setq evil-replace-state-cursor '("chocolate" (hbar . 2)))
  (setq evil-visual-state-cursor '("gray" (hbar . 2)))
  (setq evil-motion-state-cursor '("plum3" hollow))

  :config
  (when (eq seartipy-default-editing-style 'emacs)
    (setq evil-emacs-state-modes (append evil-insert-state-modes evil-emacs-state-modes))
    (setq evil-insert-state-modes nil)

    (setq evil-emacs-state-modes (append evil-motion-state-modes evil-emacs-state-modes))
    (setq evil-motion-state-modes nil))
  (evil-mode))

(use-package hybrid-mode
  :ensure nil
  :diminish hybrid-mode " Ⓔh"

  :init
  (with-eval-after-load 'spaceline
    (setq spaceline-evil-state-faces(cons '(hybrid . spaceline-evil-insert) spaceline-evil-state-faces)))
  (custom-set-variables '(hybrid-mode-default-state 'hybrid))

  :config
  (when (eq seartipy-default-editing-style 'hybrid)
    (hybrid-mode)))

(use-package bind-map
  :config
  (bind-map seartipy-mode-map
    :keys ((concat "M-" seartipy-leader-key) seartipy-emacs-leader-key)
    :evil-keys (seartipy-leader-key)
    :evil-states (normal visual motion)))

(defmacro my/set-key (&rest bindings)
  `(bind-map-set-keys seartipy-mode-map ,@bindings))
(put 'my/set-key 'lisp-indent-function 'defun)
(defalias 'evil-leader/set-key 'my/set-key)

(defun my/init-leader-map (mode keymap leaders emacs-leaders)
  (or (boundp keymap)
      (progn
        (eval
         `(bind-map ,keymap
            :major-modes (,mode)
            :keys ,emacs-leaders
            :evil-keys ,leaders
            :evil-states (normal motion visual)))
        (boundp keymap))))

(defmacro my/set-key-for-mode (mode &rest bindings)
  (let ((keymap (intern (format "%s-bs-map" mode)))
        (emacs-leaders '((concat "M-" seartipy-major-mode-leader-key)
                         (concat "M-" seartipy-leader-key " m")
                         (concat seartipy-emacs-leader-key " m")
                         seartipy-major-mode-emacs-leader-key))
        (leaders '(seartipy-major-mode-leader-key
                   (concat seartipy-leader-key " m"))))
    (my/init-leader-map mode keymap leaders emacs-leaders)
    `(bind-map-set-keys ,keymap ,@bindings)))
(put 'my/set-key-for-mode 'lisp-indent-function 'defun)
(defalias 'evil-leader/set-key-for-mode 'my/set-key-for-mode)

(use-package evil-escape
  :diminish evil-escape-mode

  :init
  (with-eval-after-load 'evil
    (evil-escape-mode)))

;; 2-char searching ala vim-sneak & vim-seek, for evil-mode and Emacs
(use-package evil-snipe
  :defer t
  :diminish evil-snipe-local-mode

  :init
  (with-eval-after-load 'evil
    (evil-snipe-mode)
    (evil-snipe-override-mode)))

;; you will be surrounded (surround.vim for evil, the extensible vi layer)
(use-package evil-surround
  :config
  (with-eval-after-load 'evil
    (global-evil-surround-mode)
    (evil-define-key 'visual evil-surround-mode-map "s" 'evil-surround-region)
    (evil-define-key 'visual evil-surround-mode-map "S" 'evil-substitute)))

;; Start a * or # search from the visual selection
(use-package evil-visualstar
  :defer t

  :init
  (with-eval-after-load 'evil
    (global-evil-visualstar-mode)
    (define-key evil-visual-state-map (kbd "*")
      'evil-visualstar/begin-search-forward)
    (define-key evil-visual-state-map (kbd "#")
      'evil-visualstar/begin-search-backward)))

;; Motions and text objects for delimited arguments in Evil.
(use-package evil-args
  :config
  ;; bind evil-args text objects
  (bind-key "a" 'evil-inner-arg evil-inner-text-objects-map)
  (bind-key "a" 'evil-outer-arg evil-outer-text-objects-map)

  ;; bind evil-forward/backward-args
  (bind-key "L" 'evil-forward-arg evil-normal-state-map)
  (bind-key "H" 'evil-backward-arg evil-normal-state-map)
  (bind-key "L" 'evil-forward-arg evil-motion-state-map)
  (bind-key "H" 'evil-backward-arg evil-motion-state-map))

;; Press “%” to jump between matched tags in Emacs
(use-package evil-matchit
  :config
  (global-evil-matchit-mode))

(use-package evil-iedit-state
  :init
  (evil-leader/set-key "se" 'evil-iedit-state/iedit-mode))

;; indenting based textobjects
(use-package evil-indent-plus
  :init
  (evil-indent-plus-default-bindings))

(use-package evil-nerd-commenter
  :init
  (evil-leader/set-key
    ";"  'evilnc-comment-operator
    "cl" 'evilnc-comment-or-uncomment-lines
    "ci" 'evilnc-toggle-invert-comment-line-by-line
    "cp" 'evilnc-comment-or-uncomment-paragraphs
    "ct" 'evilnc-quick-comment-or-uncomment-to-the-line
    "cy" 'evilnc-copy-and-comment-lines))

(use-package linum-relative
  :defer t
  :diminish linum-relative-mode

  :init
  (evil-leader/set-key "tr" 'linum-relative-toggle)

  :config
  (setq linum-format 'linum-relative)
  (setq linum-relative-current-symbol "")
  (linum-relative-toggle))

(use-package evil-anzu
  :diminish anzu-mode

  :init
  (setq anzu-search-threshold 1000
        anzu-cons-mode-line-p nil)
  (with-eval-after-load 'evil
    (global-anzu-mode)))

(use-package evil-tutor :defer t)



;;;; keys

(use-package which-key
  :diminish which-key-mode

  :init
  (evil-leader/set-key "hk" 'which-key-show-top-level)
  :config
  (defun my/declare-prefix (prefix description)
    (which-key-declare-prefixes
      (concat seartipy-leader-key " " prefix) description
      (concat "M-" seartipy-leader-key " " prefix) description
      (concat seartipy-emacs-leader-key " " prefix) description))

  (defun my/declare-prefix-for-mode (mode prefix description)
    (which-key-declare-prefixes-for-mode mode
      (concat seartipy-leader-key " m" prefix) description
      (concat "M-" seartipy-leader-key " m" prefix) description
      (concat seartipy-emacs-leader-key " m" prefix) description ;; not working
      (concat seartipy-major-mode-leader-key " " prefix) description
      (concat "M-" seartipy-major-mode-leader-key " " prefix) description
      (concat seartipy-major-mode-emacs-leader-key " " prefix) description))

  (defun my/declare-prefixes (key desc &rest bindings)
    (while key
      (my/declare-prefix key desc)
      (setq key (pop bindings)
            desc (pop bindings))))
  (defun my/declare-prefixes-for-mode (mode key desc &rest bindings)
    (while key
      (my/declare-prefix-for-mode mode key desc)
      (setq key (pop bindings)
            desc (pop bindings))))

  (put 'my/declare-prefix 'lisp-indent-function 'defun)
  (put 'my/declare-prefixes 'lisp-indent-function 'defun)
  (put 'my/declare-prefix-for-mode 'lisp-indent-function 'defun)
  (put 'my/declare-prefixes-for-mode 'lisp-indent-function 'defun)

  (my/declare-prefixes
    "a"   "applications"
    "b"   "buffers"
    "bm"  "move"
    "c"   "compile/comments"
    "C"   "capture/colors"
    "e"   "errors"
    "f"   "files"
    "fC"  "files/convert"
    "g"   "git/versions-control"
    "h"   "helm/help/highlight"
    "hd"  "help-describe"
    "i"   "insertion"
    "il"  "lorem"
    ;; "j"   "join/split"
    ;; "k"   "lisp"
    ;; "kd"  "delete"
    ;; "kD"  "delete-backward"
    ;; "k`"  "hybrid"
    "n"   "narrow/numbers"
    "p"   "projects"
    ;; "p$"  "projects/shell"
    "q"   "quit"
    "r"   "registers/rings"
    "Re"  "elisp"
    "Rp"  "pcre"
    "s"   "search/symbol"
    "sa"  "ag"
    "sg"  "grep"
    "S"   "spell"
    "t"   "toggles"
    "tC"  "colors"
    "th"  "highlight"
    "w"   "windows"
    ;; "wp"  "popup"
    "x"   "text"
    "xa"  "align"
    "xd"  "delete"
    ;; "xl"  "lines"
    ;; "xm"  "move"
    "xt"  "transpose"
    ;; "xw"  "words"
    ;;"z"   "zoom"
    )

  (which-key-mode))

(use-package key-chord
  :config
  (key-chord-mode +1))

(use-package hydra
  :defer t

  :config
  (require 'hydra-examples))

(use-package discover-my-major :defer t)

(provide 'init-keys)
