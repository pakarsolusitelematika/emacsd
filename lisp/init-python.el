(use-package helm-pydoc
  :defer t
  :init
  (evil-leader/set-key-for-mode python-mode "hd" 'helm-pydoc))

(use-package python
  :defer t
  :init
  (defun python-setup ()
    (setq tab-width 4
          ;; auto-indent on colon doesn't work well with if statement
          electric-indent-chars (delq ?: electric-indent-chars))
    (company-mode)
    (eldoc-mode)
    (setq python-shell-interpreter "ipython"
          python-shell-interpreter-args "--simple-prompt -i"))
  (add-hook 'inferior-python-mode-hook
            (lambda () (setq indent-tabs-mode t)))
  (add-hook 'python-mode-hook 'python-setup)


  (my/declare-prefixes-for-mode 'python-mode
    "mc" "execute"

    "d" "debug"
    "h" "help"
    "g" "goto"
    "t" "test"
    "s" "send to REPL"
    "r" "refactor"
    "v" "venv")

  (evil-leader/set-key-for-mode python-mode
    ;; "db" 'python-toggle-breakpoint
    ;; "ri" 'python-remove-unused-imports
    "sb" 'python-shell-send-buffer
    "sf" 'python-shell-send-defun
    "si" 'python-shell-switch-to-shell
    "sr" 'python-shell-send-region))

(use-package pytest
  :defer t
  :commands (pytest-one
             pytest-pdb-one
             pytest-all
             pytest-pdb-all
             pytest-module
             pytest-pdb-module)
  :init (evil-leader/set-key-for-mode python-mode
          "tA" 'pytest-pdb-all
          "ta" 'pytest-all
          "tB" 'pytest-pdb-module
          "tb" 'pytest-module
          "tT" 'pytest-pdb-one ;; currently pytest-*-one does not work
          "tt" 'pytest-one
          "tM" 'pytest-pdb-module
          "tm" 'pytest-module)
  :config
  (add-to-list 'pytest-project-root-files "setup.cfg"))

(use-package pyenv-mode
  :defer t
  :init
  (evil-leader/set-key-for-mode python-mode
    "vs" 'pyenv-mode-set
    "vu" 'pyenv-mode-unset))

(use-package pyvenv
  :defer t
  :init
  (evil-leader/set-key-for-mode python-mode
    "V" 'pyvenv-workon))

(use-package anaconda-mode
  :diminish anaconda-mode
  :defer t
  :init
  (add-hook 'python-mode-hook 'anaconda-mode)
  (add-hook 'python-mode-hook 'anaconda-eldoc-mode))

(use-package company-anaconda
  :defer t
  :init
  (setq anaconda-mode-installation-directory
        (concat seartipy-cache-directory "anaconda-mode"))
  (with-eval-after-load 'company
    (add-to-list 'company-backends '(company-anaconda :with company-capf)))
  (evil-leader/set-key-for-mode python-mode
    "hh" 'anaconda-mode-show-doc
    "gg" 'anaconda-mode-find-definitions
    "ga" 'anaconda-mode-find-assignments
    "gu" 'anaconda-mode-find-references))

(provide 'init-python)
