(use-package project-local-variables :defer t)

(use-package seq)
(use-package dash)
(use-package fullframe)



(use-package server
  :config
  (unless (server-running-p)
    (server-start)))



(use-package uniquify
  :ensure nil
  :init
  (setq uniquify-buffer-name-style 'reverse)
  (setq uniquify-separator " • ")
  (setq uniquify-after-kill-buffer-p t)
  (setq uniquify-ignore-buffers-re "^\\*"))



(use-package ibuffer
  :init
  (setq ibuffer-filter-group-name-face 'font-lock-doc-face)
  (setq-default ibuffer-show-empty-filter-groups nil)

  :config
  (fullframe ibuffer ibuffer-quit))



(use-package dired+ :defer t)
(use-package dired-sort :defer t)

(use-package dired
  :ensure nil
  :init
  (setq-default diredp-hide-details-initially-flag nil
                dired-dwim-target t)

  :config
  (require 'dired+)
  (require 'dired-sort)
  (when (fboundp 'global-dired-hide-details-mode)
    (global-dired-hide-details-mode -1))
  (setq dired-recursive-deletes 'top)
  (define-key dired-mode-map [mouse-2] 'dired-find-file))



(provide 'init-essential)
