(use-package spacemacs-theme :defer t)
(use-package dracula-theme :defer t)
(use-package solarized-theme :defer t)
(use-package monokai-theme :defer t)
(use-package material-theme :defer t)
(use-package leuven-theme :defer t)
(use-package darkokai-theme :defer t)
(use-package color-theme-sanityinc-tomorrow :defer t)
(use-package nord-theme :defer t)



;; Locales (setting them earlier in this file doesn't work in X)

(defun utf8-locale-p (v)
  "Return whether locale string V relates to a UTF-8 locale."
  (and v (string-match "UTF-8" v)))

(defun locale-is-utf8-p ()
  "Return t iff the \"locale\" command or environment variables prefer UTF-8."
  (or (utf8-locale-p (and (executable-find "locale") (shell-command-to-string "locale")))
      (utf8-locale-p (getenv "LC_ALL"))
      (utf8-locale-p (getenv "LC_CTYPE"))
      (utf8-locale-p (getenv "LANG"))))

(when (or window-system (locale-is-utf8-p))
  (setq utf-translate-cjk-mode nil) ; disable CJK coding/encoding (Chinese/Japanese/Korean characters)
  (set-language-environment 'utf-8)
  (setq locale-coding-system 'utf-8)
  (set-default-coding-systems 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-selection-coding-system (if (eq system-type 'windows-nt) 'utf-16-le 'utf-8))
  (prefer-coding-system 'utf-8))




(defun seartipy/set-font (font)
  "Set the FONT as frame font, if the font is available."
  (let ((font-name (apply #'concat (-interpose " " (butlast (split-string font))))))
    (if (-contains? (font-family-list) font-name)
        (set-frame-font font)
      (message (format "%s not available on this system" font-name)))))

(when *is-a-mac*
  (seartipy/set-font seartipy-mac-font))

(when *is-a-linux*
  (seartipy/set-font seartipy-linux-font))

(when *is-a-windows*
  (setq inhibit-compacting-font-caches t)
  (seartipy/set-font seartipy-windows-font))

(setq darkokai-mode-line-padding 1) ;; Default mode-line box width
(load-theme seartipy-theme t)

(when seartipy-fullscreen-at-startup
  (toggle-frame-fullscreen))

(when seartipy-line-numbers
  (linum-relative-global-mode))

(when seartipy-highlight-line
  (global-hl-line-mode +1))

;; must be loaded after most things

(use-package spaceline
  :config
  (defadvice load-theme (after reset-powerline-with-eval-after-load-theme activate)
    (powerline-reset))

  (setq-default spaceline-window-numbers-unicode t)
  (setq spaceline-highlight-face-func 'spaceline-highlight-face-evil-state)
  (setq spaceline-display-default-perspective nil)
  (setq spaceline-byte-compile t)
  (require 'spaceline-config)
  (spaceline-helm-mode)
  ;; (spaceline-spacemacs-theme)
  (spaceline-install
   '(((persp-name
       window-number)
      :fallback evil-state
      :separator "|"
      :face highlight-face)
     ((point-position
       line-column)
      :separator " | ")
     (buffer-modified buffer-id remote-host)
     anzu
     major-mode
     ((flycheck-error flycheck-warning flycheck-info)
      :when active)
     (((minor-modes :separator spaceline-minor-modes-separator)
       process)
      :when active)
     projectile-root
     (version-control :when active))

   `(selection-info
     (global :when active)
     buffer-position))
  (setq-default mode-line-format '("%e" (:eval (spaceline-ml-main))))
  (add-hook 'after-init-hook 'spaceline-compile))


(provide 'init-ui)
