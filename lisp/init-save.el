;;;; saving emacs session

(use-package desktop
  :init
  (setq desktop-path (list seartipy-cache-directory))
  (custom-set-variables '(desktop-restore-eager 5))

  :config
  (when (and (seq-contains seartipy-save-session 'desktop)
             (not (seq-contains seartipy-save-session 'workspace)))
    (desktop-save-mode t)))

(use-package persp-mode
  :diminish persp-mode
  :init
  (setq persp-auto-resume-time 1
        persp-nil-name "Default"
        persp-reset-windows-on-nil-window-conf nil
        persp-set-last-persp-for-new-frames nil
        persp-save-dir (concat seartipy-cache-directory "layouts/"))
  (custom-set-variables '(persp-keymap-prefix (kbd "C-c l")))

  :config
  (when (seq-contains seartipy-save-session 'workspace)
    (persp-mode)))

(use-package savehist
  :init
  (setq savehist-file (concat seartipy-cache-directory "savehist")
        enable-recursive-minibuffers t ; Allow commands in minibuffers
        history-length 1000
        savehist-additional-variables '(mark-ring
                                        global-mark-ring
                                        search-ring
                                        regexp-search-ring
                                        extended-command-history)
        savehist-autosave-interval 60)
  (when (seq-contains seartipy-save-session 'savehist)
    (savehist-mode t)))

(use-package saveplace
  :if (seq-contains seartipy-save-session 'saveplace)

  :init
  (setq save-place t
        save-place-file (concat seartipy-cache-directory "places"))

  :config
  (when (fboundp 'save-place-mode)
    (save-place-mode)))

(use-package recentf
  :init
  (setq recentf-save-file (concat seartipy-cache-directory "recentf")
        recentf-max-saved-items 100
        recentf-auto-save-timer (run-with-idle-timer 600 t 'recentf-save-list)
        recentf-exclude '("/tmp/" "/ssh:"))

  :config
  (when (seq-contains seartipy-save-session 'recentf)
    (recentf-mode +1)))

(provide 'init-save)

