;; (package-initialize)

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(add-to-list 'load-path (expand-file-name "vendor" user-emacs-directory))

(require 'init-default-settings)
(require 'init-melpa)
(require 'init-essential)
(require 'init-save)
(require 'init-keys)
(require 'init-helm)

(require 'init-editing)
(require 'init-git)
(require 'init-search)
(require 'init-keybindings)

(require 'init-layers)

(require 'init-misc)
(require 'init-ui)

(when (file-exists-p "~/.emacs-post-local.el")
  (load-file "~/.emacs-post-local.el"))

;; Local Variables:
;; coding: utf-8
;; no-byte-compile: t
;; End:
